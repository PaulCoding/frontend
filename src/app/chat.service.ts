import { Injectable } from "@angular/core";
import { Observable, Subject, from } from "rxjs";
import { WebsocketService } from "./websocket.service";
//import {map} from 'rxjs/operator/map';
import { map } from 'rxjs/operators';
import { response } from 'express';
import {User} from './app.user';
import {Message} from './app.message';
import { Person } from './Modules/app.Person';
import {msgsend} from './Modules/app.msgsend';
import {room} from './Modules/app.room';
@Injectable()
export class ChatService {
  public messages: Subject<String>;
  public thisUser:String = "";

  public currentMessages: Message[] = [];
  public selectedUser:User = null;
  public isRoom:boolean = false;

  public connect(url:string, userName:string)
  {
    this.thisUser = userName;
    this.messages = <Subject<String>>this.wsService.connect(url).pipe(map(
      (response: MessageEvent): String => {
        var json = JSON.parse(response.data);
        if(json["type"] == "msg")
        {
          this.newMsg(response.data);
        }
        else if(json["type"] == "groupmsg")
        {
          this.newGroupMSG(response.data);
        }
        return "";
      }
    ));


  }


  public selectRoom(room:room)
  {
    this.currentMessages = [];
    this.isRoom = true;
    this.selectedUser = new User(room.name, "", room.id);
  }
  public selectUser(id:string, userList:Person[]){

      this.isRoom = false;
      userList.forEach(element => {
          if(element.name == id)
          {
            this.selectedUser = new User(element.name, element.profileImage, -1);
            return;
          }
      });
      this.currentMessages = [];
  }
  n:Number;
  public newGroupMSG(msg:string)
  {
    var split = JSON.parse(msg);

    var m = new Message(1,split["Sender"], this.thisUser, split["date"], split["msg"]);
    console.log(split["Sender"]);
    this.n = split["Group"];
    if(this.n == this.selectedUser.Id)
    {
      console.log(m);
      this.currentMessages.push(m);
    }
  }

  public newMsg(msg:string)
  {
    var split = JSON.parse(msg);

    var m = new Message(1,split["Sender"], this.thisUser, split["date"], split["msg"]);
    console.log(split["Sender"]);
    if(m.senderID == this.selectedUser.Name || m.recieverID == this.selectedUser.Name)
    {
      console.log(m);
      this.currentMessages.push(m);
    }
  }

  public send(text:string)
  {
    if(!this.isRoom){
      var send= new msgsend("msg", this.selectedUser.Name, text);
      var m = new Message(0,this.thisUser, this.selectedUser.Name, new Date().toString(), text);
      this.messages.next(JSON.stringify(send));
      this.currentMessages.push(m);
    }
    else
    {
      var send= new msgsend("groupmsg", this.selectedUser.Id+"", text);
      var m = new Message(0,this.thisUser, this.selectedUser.Name, new Date().toString(), text);
      this.messages.next(JSON.stringify(send));
      this.currentMessages.push(m);
    }
  }
  //{"type":"groupmsg","target":"test","msg":"group"}

  public addUser(text:string){

  }

  constructor(public wsService: WebsocketService) {

  }
}
