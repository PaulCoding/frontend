import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Person } from './Modules/app.Person';
import { msgsend } from './Modules/app.msgsend';
import { group } from 'console';
@Injectable({
  providedIn: 'root'
})
export class RestService {
  http: HttpClient;
  url: string = 'http://localhost:8080/chat/rest';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
    'Authorization':'' })
  };
  constructor(http: HttpClient) {
    this.http = http;
  }

  setToken(token:String)
  {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token })
    };
  }

  getUsers() {
    return this.http.get<any>(this.url + '/User/allUsers', this.httpOptions);
  }



  public getRooms(id: Number) {
    return this.http.get<any>(this.url + "/Person/rooms/" + id, this.httpOptions);
  }

  public getRoomMSG(id: Number) {
    return this.http.get<any>(this.url + "/Room/" + id, this.httpOptions);
  }

  public getUserByName(name: String) {
    return this.http.get<any>(this.url + "/UserByName/" + name, this.httpOptions);
  }


  public login(password: String, userName: String){
    return this.http.post<any>(this.url+"/login",{
      "username":userName,
      "password":password
    }, this.httpOptions)
  }

  public updateImage(imageUrl: String, userID: Number)
  {
    return this.http.post<any>(this.url+ "/ImageUpdate/"+userID,{
      "imageUpdate": imageUrl
    });
  }
  public createGroup(groupName:String)
  {
    return this.http.post<any>(this.url + "/Room/create/Create", {
      "name": groupName
    }, this.httpOptions);
  }
  public leaveGroup(groupName: String, userID:Number) {
    return this.http.post<any>(this.url + "/LeaveRoom/" + userID ,
    {
        "name": groupName

    },this.httpOptions);
  }

  public joinGroup(groupName: String, userID: Number) {
    console.log(userID);
    console.log(groupName);
    return this.http.post<any>(this.url + "/Room/" + userID ,
    {
        "name": groupName

    },this.httpOptions);
  }

  getMessages(sender: String, reciever: String) {
    return this.http.get<any>(this.url + '/User/' + sender + '/' + reciever,this.httpOptions);
  }
}
