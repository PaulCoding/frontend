import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { webSocket } from "rxjs/webSocket";
import { WebsocketService } from "./websocket.service";
import { ChatService } from "./chat.service";
import { RestService } from './rest.service';
import { Person } from './Modules/app.Person';
import { Message } from './app.message';
import { member } from './Modules/app.member';
import { room } from './Modules/app.room';

import { from } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [WebsocketService, ChatService]

})
export class AppComponent {
  title = 'whatsapp-clone';
  serverIP = "ws://localhost:8080/chat";
  UserName = "paul1";
  Password = "pass1";
  Image = "imageUrl";
  groupName = "";
  msgText = "";

  public people: Person[];
  public membership: member[];

  constructor(public chatService: ChatService, public restService: RestService) {
  }

  user: Person;

  updateList() {
    this.people = [];
    this.getUsers();
    this.ReloadGroup();
  }
  connect() {
    var newURL = this.serverIP + "/" + this.UserName + "/" + this.Password;
    this.chatService.connect(newURL, this.UserName);
    this.chatService.messages.subscribe(msg => {
      console.log("Response from websocket: " + msg);

    });

    this.restService.login(this.Password, this.UserName).subscribe(val => {
      console.log(val.token);
      this.restService.setToken(val.token);


      this.getUsers();
      this.ReloadGroup();
    });


    this.Image = this.user.profileImage.toString();
  }


  getUsers() {
    this.restService.getUsers().subscribe(val => {
      this.people = val;
      console.log(this.people);
    });
  }

  leaveGroup(groupName:String)
  {
    this.restService.leaveGroup(groupName, this.user.id).subscribe(val=>{
      console.log(val);
      this.ReloadGroup();
    })
  }

  addGroup() {
    this.restService.createGroup(this.groupName).subscribe(val => {
      this.restService.joinGroup(this.groupName, this.user.id).subscribe(val => {

        this.ReloadGroup();
      })
    });
  }


  joinGroup() {
    this.restService.joinGroup(this.groupName, this.user.id).subscribe(val => {
      this.ReloadGroup();
    })
  }

  ReloadGroup() {
    this.membership = [];
    this.restService.getUserByName(this.chatService.thisUser).subscribe(val => {
      this.user = val;
      console.log(this.user);
      this.restService.getRooms(this.user.id).subscribe(val2 => {
        console.log(val2);
        this.membership = val2;
        console.log(this.membership);
      })
    });
  }

  imageU() {
    this.user.profileImage = this.Image;
    this.restService.updateImage(this.Image, this.user.id).subscribe(val => {
    })
  }
  send() {
    this.chatService.send(this.msgText);
  }
  msgs: Message[];
  selectUser(id: string, isUser: boolean) {
    if (isUser) {
      this.chatService.selectUser(id, this.people);

      this.restService.getMessages(this.chatService.thisUser, id).subscribe(val => {
        this.msgs = val;
        this.msgs.forEach(element => {
          this.chatService.currentMessages.push(element);
        });
      })
    }
    else {
      var room = this.membership.find(x => x.room.name == id).room;
      console.log(room);
      this.chatService.selectRoom(room);
      this.restService.getRoomMSG(room.id).subscribe(val => {
        this.msgs = val;
        console.log(room.id);
        this.msgs.forEach(element => {
          console.log(element.senderID)
          var msg = new Message(1, element.senderID, "member", "date", "string");
          console.log(msg);
          this.chatService.currentMessages.push(element);

        });
      })
    }
  }

  getUser(id: Number) {
  }

  update() {
    this.chatService.messages.next("users");
    console.log("msg send");
  }
}
